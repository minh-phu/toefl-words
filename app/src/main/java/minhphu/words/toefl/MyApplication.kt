package minhphu.words.toefl

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.google.android.gms.ads.MobileAds
import minhphu.words.toefl.utils.CipherHelper
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import io.fabric.sdk.android.Fabric

class MyApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())
        MobileAds.initialize(applicationContext, getString(R.string.app_ads_id))
        CipherHelper(getSecretKey())
    }

    private external fun getSecretKey(): String

    companion object {
        init {
            System.loadLibrary("native-lib")
        }
    }
}
