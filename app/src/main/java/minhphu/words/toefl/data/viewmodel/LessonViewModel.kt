package minhphu.words.toefl.data.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import minhphu.words.toefl.data.database.entities.Lesson
import minhphu.words.toefl.data.database.repository.LessonRepository

class LessonViewModel(lessonRepository: LessonRepository) : ViewModel() {

    var mListLesson: LiveData<List<Lesson>> = lessonRepository.getListLesson()

}