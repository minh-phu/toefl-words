package minhphu.words.toefl.data.model

class ResultPractice(var question: String?, var answer: String?, var userAnswer: String?) {

    val isUserChooseCorrect: Boolean
        get() = answer == userAnswer
}
