package minhphu.words.toefl.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import minhphu.words.toefl.utils.CipherHelper

@Entity
class Lesson {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "score_remember_1")
    var scoreRememberOne: Int = 0

    @ColumnInfo(name = "score_remember_2")
    var scoreRememberTwo: Int = 0

    @ColumnInfo(name = "score_listen_1")
    var scoreListenOne: Int = 0

    @ColumnInfo(name = "score_listen_2")
    var scoreListenTwo: Int = 0

    @ColumnInfo(name = "image")
    var image: String? = null

    @Ignore
    var nameDecrypt: String = ""
        get() = name?.let { CipherHelper.decrypt(it) }.toString()

}