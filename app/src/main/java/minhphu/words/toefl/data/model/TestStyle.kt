package minhphu.words.toefl.data.model


enum class TestStyle {
    VIEW_LESSON, VOCABULARY_TEST, MEANING_TEST, CHOOSE_PRONOUNCE, LISTEN_AND_CHOOSE
}