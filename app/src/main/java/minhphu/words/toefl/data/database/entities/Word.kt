package minhphu.words.toefl.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import minhphu.words.toefl.utils.CipherHelper

@Entity
class Word {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "lesson_id")
    var lessonId: Int? = null

    @ColumnInfo(name = "en")
    var en: String? = null

    @ColumnInfo(name = "pron")
    var pron: String? = null

    @ColumnInfo(name = "description")
    var description: String? = null

    @ColumnInfo(name = "exam")
    var exam: String? = null

    @ColumnInfo(name = "image")
    var image: String? = null

    @ColumnInfo(name = "sound")
    var sound: String? = null

    @Ignore
    var enEncrypt: String = ""
        get() = CipherHelper.encrypt(en!!)

    @Ignore
    var enDecrypt: String = ""
        get() = CipherHelper.decrypt(en!!)
    @Ignore
    var descDecrypt: String = ""
        get() = CipherHelper.decrypt(description!!).replace("\\,", ", ")
            .replace("&nbsp;", "")
    @Ignore
    var examDecrypt: String = ""
        get() = CipherHelper.decrypt(exam!!).replace("\\,", ", ")
            .replace("&nbsp;", "")
            .replace("div", "")
}