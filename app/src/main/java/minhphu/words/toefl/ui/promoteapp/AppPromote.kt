package minhphu.words.toefl.ui.promoteapp

import android.content.Context
import minhphu.words.toefl.R

class AppPromote(
    var appName: String,
    var appIcon: Int,
    var appUrl: String,
    var isMoreApp: Boolean = false
) {
    companion object {
        fun getListPromoteApp(context: Context): ArrayList<AppPromote> {
            val listMoreApp = ArrayList<AppPromote>()

            listMoreApp.add(
                AppPromote(
                    "TOEFL Practice Test",
                    R.drawable.toefl_test_256,
                    context.getString(R.string.package_toefl_test)
                )
            )

            listMoreApp.add(
                AppPromote(
                    "TOEFL Essay",
                    R.drawable.essay_256,
                    context.getString(R.string.package_essay)
                )
            )

            listMoreApp.add(
                AppPromote(
                    "English Grammar",
                    R.drawable.grammar_icon_256,
                    context.getString(R.string.package_grammar)
                )
            )
            listMoreApp.add(
                    AppPromote(
                            "Vocabulary Builder",
                            R.drawable.voca_builder_256,
                            context.getString(R.string.package_vocabulary_builder)
                    )
            )
            listMoreApp.add(
                AppPromote(
                    "More App",
                    R.drawable.icon_dev,
                    context.getString(R.string.dev_page), true
                )
            )
            return listMoreApp
        }
    }
}