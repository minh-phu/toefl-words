package minhphu.words.toefl.ui.listword

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.speech.RecognizerIntent
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_word.*
import minhphu.words.toefl.R
import minhphu.words.toefl.data.database.entities.Word
import minhphu.words.toefl.ui.base.BaseFragment
import minhphu.words.toefl.utils.AdUtil
import minhphu.words.toefl.utils.Constants
import minhphu.words.toefl.utils.Utils
import minhphu.words.toefl.utils.mediaplayer.MediaPlayerManager
import java.util.*

class WordFragment : BaseFragment(), BaseFragment.MyOnClickListener {

    lateinit var word: Word

    companion object {

        const val FOLDER_WORD = "word/"
        const val REQ_CODE_SPEECH_INPUT = 100

        fun newInstance(word: Word): WordFragment {
            val fragmentWord = WordFragment()
            fragmentWord.word = word
            return fragmentWord
        }
    }

    override fun getFragmentLayout(): Int = R.layout.fragment_word

    override fun updateView() {
        AdUtil.showNativeAdFbSmall(
            requireContext(),
            adsContainer,
            R.string.adsFb_ListWord,
            R.string.adsGg_ListWord
        )

        tvVocabulary.text = Utils.fromHtml(word.enDecrypt)
        tvMeaning.text = Utils.fromHtml(word.descDecrypt)
        tvPronoun.text = Utils.fromHtml(word.pron)
        tvExample.text = Utils.fromHtml(word.examDecrypt)
        word.image?.let { wordImage ->
            imgWords.setImageDrawable(
                Utils.getDrawableFromAsset(
                    requireContext(),
                    "$FOLDER_WORD$wordImage.jpg"
                )
            )
        }
        setEventClick(listOf(btnSpeakVocabulary, btnOpenVoice), this)
    }

    override fun eventClick(v: View) {
        when (v.id) {
            R.id.btnSpeakVocabulary -> word.sound?.let {
                MediaPlayerManager.instance.play(
                    requireContext(),
                    it
                )
            }
            R.id.btnOpenVoice -> openVoice()
        }
    }

    private fun openVoice() {
        try {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
            )
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
            intent.putExtra(
                RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt)
            )
            try {
                startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
            } catch (a: ActivityNotFoundException) {
                Toast.makeText(
                    activity!!.applicationContext,
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT
                ).show()
            }

        } catch (e: NullPointerException) {
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQ_CODE_SPEECH_INPUT -> {
                try {
                    if (resultCode == Activity.RESULT_OK && null != data) {
                        val result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                        if (result[0].toLowerCase() == word.enDecrypt.toLowerCase()) {
                            MediaPlayerManager.instance.play(
                                requireContext(),
                                Constants.FILE_CORRECT
                            )
                        } else {
                            MediaPlayerManager.instance.play(requireContext(), Constants.FILE_WRONG)
                            val message = getString(R.string.txt_result) + " : " + result[0]
                            val actionName = getString(R.string.txt_try_again)
                            showMessage(
                                message,
                                Snackbar.LENGTH_LONG,
                                actionName,
                                object : CallbackSnackBar {
                                    override fun onAction() {
                                        openVoice()
                                    }
                                })
                        }
                    }
                } catch (ignored: NullPointerException) {
                }

            }
        }
    }

}
