package minhphu.words.toefl.utils

import android.content.Context
import minhphu.words.toefl.data.database.AppDatabase
import minhphu.words.toefl.data.database.daos.LessonDao
import minhphu.words.toefl.data.database.daos.WordDao
import minhphu.words.toefl.data.database.repository.LessonRepository
import minhphu.words.toefl.data.viewmodel.LessonViewModelFactory

object InjectorUtils {
    private fun getLessonRepository(context: Context): LessonRepository {
        return LessonRepository.getInstance(provideLessonDao(context))
    }

    fun provideLessonViewModelFactory(context: Context): LessonViewModelFactory {
        val repository = getLessonRepository(context)
        return LessonViewModelFactory(repository)
    }

    fun provideLessonDao(context: Context): LessonDao {
        return AppDatabase.getInstance(context).lessonDao()
    }

    fun provideWordDao(context: Context): WordDao {
        return AppDatabase.getInstance(context).wordDao()
    }
}
